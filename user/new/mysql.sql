# phpMyAdmin SQL Dump
# 

# --------------------------------------------------------

#
# Структура таблицы `messages`
#

CREATE TABLE `messages` (
`id` int(9) NOT NULL auto_increment,
`author` varchar(15) NOT NULL default '',
`poluchatel` varchar(15) NOT NULL default '',
`date` date NOT NULL default '0000-00-00',
`text` text NOT NULL,
PRIMARY KEY (`id`)
) TYPE=MyISAM AUTO_INCREMENT=8 ;

#
# Дамп данных таблицы `messages`
#

CREATE TABLE `contracts` (
`id` int(9) NOT NULL auto_increment,
`user_id` int(9) NOT NULL,
`project_id` int(9) NOT NULL,
`startdate` date NOT NULL default '0000-00-00 00:00:00',
`finishdate` date NOT NULL default '0000-00-00 00:00:00',
`time` int(10) NOT NULL default '84600',
`investcount` int(9) NOT NULL default '0',
`activation` int(1) NOT NULL default '0',
PRIMARY KEY (`id`)
)

CREATE TABLE `projects` (
`id` int(9) NOT NULL auto_increment,
`name` varchar(255) NOT NULL default '',
`short_descr` varchar(255) NOT NULL default '',
`theme` varchar(255) NOT NULL default '',
`descr` varchar(255) NOT NULL default '',
`content` text NOT NULL default '',
`user_id` int(9) NOT NULL default '0',
`startdate` date NOT NULL default '0000-00-00 00:00:00',
`finishdate` date NOT NULL default '0000-00-00 00:00:00',
`time` int(10) NOT NULL default '86400',
`activation` int(1) NOT NULL default '0',
PRIMARY KEY (`id`)
)




# --------------------------------------------------------

#
# Структура таблицы `oshibka`
#

CREATE TABLE `oshibka` (
`ip` varchar(12) NOT NULL default '',
`date` datetime NOT NULL default '0000-00-00 00:00:00',
`col` int(1) NOT NULL default '0'
) TYPE=MyISAM;

#
# Дамп данных таблицы `oshibka`
#


# --------------------------------------------------------

#
# Структура таблицы `users`
#

CREATE TABLE `users` (
`id` int(11) NOT NULL auto_increment,
`login` varchar(15) NOT NULL default '',
`password` varchar(255) NOT NULL default '',
`avatar` varchar(255) NOT NULL default '',
`email` varchar(255) NOT NULL default '',
`activation` int(1) NOT NULL default '0',
`date` datetime NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (`id`)
) TYPE=MyISAM AUTO_INCREMENT=41 ;

#
# Дамп данных таблицы `users`
#
