<?php 
require '../frame/modules/user/referal.php';
require 'section/header.php'; 
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Регистрация аккаунта</div>
      <div class="card-body">
        <form method="post" action="new/save_user">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Логин*</label>
                <input class="form-control" name="login" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Придумайте логин" required>
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName">Телефон</label>
                <input class="form-control" name="tel" id="exampleInputLastName" type="tel" aria-describedby="nameHelp" placeholder="Введите ваш номер телефона">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email*</label>
            <input class="form-control" name="email" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Введите email" required>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Пароль*</label>
                <input class="form-control" name="password" id="exampleInputPassword1" type="password" placeholder="Пароль" required minlength="6">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Повторите пароль*</label>
                <input class="form-control" name="password2" id="exampleConfirmPassword" type="password" placeholder="Повторите пароль" required minlength="6">
                <input type="text" style="display: none;" name="ref" value="<?php echo $_SESSION['ref']; ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <p><label for="exampleConfirmCode">Введите код с картинки*</label><br>
                <p><img src="new/code/my_codegen.php"></p></div>
              <div class="col-md-6">
                <p>
                  <input type="text" id="exampleConfirmCode" class="form-control" name="code" required placeholder="Введите код с картинки"></p>
                <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" required>Принимаю 
<a  data-toggle="modal" data-target="#termsModal" href="">Условия пользования</a>
              </label>
            </div>
              </div>
            </div>
          </div>
           

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                
              </div>
            </div>
          </div>
          <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn-primary btn-block">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login">Авторизация</a>
          <a class="d-block small" href="forgot-password">Забыли пароль?</a>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Условия пользования</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Текст условий пользования.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>