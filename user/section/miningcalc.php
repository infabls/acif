<?php 
// цена фермы в долларах
$farmprice = 9500;

// общий хешрейт в мегахешах
$hashrate = 430;

// стоимость мегахеша в долларах
$hashprice = $farmprice/$hashrate;

// количество хешей у клиента
$yourhash = round($row['investcount']/$hashprice, 2);

//наша комиссия в процентах
$commission = 20;

//потребление ватт на мегахеш
$consumption = 8;

//цена за киловатт/час в центах
$centwattprice = 6;

//стоимость эфира в долларах. берём с coinmarketcap
$etherprice = '560';

//цена за киловатт.час в этерии
$etherwattprice = $centwattprice/100/$etherprice ;

//цена за электричество в этерии
$electric = ($hashrate * $consumption)/1000*24*$etherwattprice;

//добыча. сколько добыто в день в эфире. берём из ethermine
$extraction = '';

//сумма к выплатам
$pay = ($extraction - $electric)*0.8;

//выплата конкретному персонажу
$paytoclient = $farmprice/$row['investcount']
 ?>