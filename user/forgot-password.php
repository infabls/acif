<?php require 'section/header.php'; ?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Восстановление пароля</div>
      <div class="card-body">
        <div class="text-center mt-4 mb-5">
          <h4>Забыли свой пароль?</h4>
          <p>Введите свой Логин и Email адрес, и мы отправим на него инструкии по восстановлению пароля.</p>
        </div>
        <form action="new/send_pass" method="POST">
          <div class="form-group">
            <input class="form-control" id="exampleInputLogin" type="text" name="login" aria-describedby="loginHelp" placeholder="Введите логин" required><br>
            <input class="form-control" id="exampleInputEmail1" type="email" name="email" aria-describedby="emailHelp" placeholder="Введите email адрес" required>
          </div>
          <input type="submit" value="Восстановить пароль" class="btn btn-primary btn-block">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register">Регистрация</a>
          <a class="d-block small" href="login">Авторизация</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
