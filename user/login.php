<?php 
session_start();
require 'section/header.php'; ?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Авторизация</div>
      <div class="card-body">
        <form action="new/testreg" method="POST">
          <div class="form-group">
            <label for="exampleInputLogin">Логин</label>
            <input class="form-control" name="login" id="exampleInputLogin" type="text" aria-describedby="emailHelp" placeholder="Введите ваш логин" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Пароль</label>
            <input class="form-control" name="password" id="exampleInputPassword1" type="password" placeholder="Пароль" required>
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Запомнить пароль</label>
            </div>
          </div>
          <input type="submit" value="Войти" class="btn btn-primary btn-block">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register">Регистрация</a>
          <a class="d-block small" href="forgot-password">Забыли пароль?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
