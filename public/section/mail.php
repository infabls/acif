<!-- ****************************** Subscribe section ************************** -->
	<section id="subscribe" class="block">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Новости рынка Криптовалют</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			<div class="row subscription-wraper">
				<div class="col-md-6 ">
					<p>Получайте свежие новости о рынке криптовалют из нашего блога на свою электронную почту. Узнавайте первыми об инсайдах, новинках, тенденциях рынка вместе с нашей платформой.</p>
					<ul class="feature-list subscription-ul">
						<li><i class="ion-university"></i> Куча информации для новичков</li>
						<li><i class="ion-chatbubbles"></i> Обсуждения и живое общение</li>
						<li><i class="ion-erlenmeyer-flask"></i> Мнение экспертов о рынке криптовалют</li>
						<li><i class="ion-clock"></i> Последние новости и инсайды</li>
					</ul>
				</div>
				<div class="col-md-6 subscription-area">
					<div class="subscription-content">
						<p class="title-text">Подпишись</p>
						<p>Только полезная информация. Никакого спама</p>
					</div>
					<form id="subscription-form" class="subscription-form" role="search">
					    <div class="input-group add-on">
					      <input type="email" class="form-control subscription-form-control subscriber-email" placeholder="Email" name="srch-term" id="srch-term">
					      <div class="input-group-btn">
					        <button id="subscribe-button" class="btn btn-default" type="submit"><i class="ion-ios-paperplane"></i></button>
					      </div>
					    </div>
					</form>

						<!-- SUCCESS OR ERROR MESSAGES -->			
						<div id="subscription-response" class="subscription-success"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 wow animated bounceInUp">
					
				</div>
			</div>
		</div>
	</section>