<!-- ****************************** Pricing section ************************** -->
	<section id="pricing" class="block">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Lucy Price plan</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="pricing-table">
						<li class="wow flipInY animated" style="visibility: visible;">
							<h3>Standard</h3>
							<ul class="benefits-list">
								<li>Responsive</li>
								<li>Documentation</li>
								<li class="not">Multiplatform</li>
								<li class="not">Video background</li>
								<li class="not">Support</li>
								<li class="special-price">from <strong>$2.99</strong> /month</li>
							</ul>
							<a href="#" class="btn btn-default btn-download">get started now</a>
						</li>
						<li class="silver wow flipInY animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
							<h3>Sliver</h3>
							<ul class="benefits-list">
								<li>Responsive</li>
								<li>Documentation</li>
								<li>Multiplatform</li>
								<li class="not">Video background</li>
								<li class="not">Support</li>
								<li class="special-price">from <strong>$4.99</strong> /month</li>
							</ul> 
							<a href="#" class="btn btn-default btn-download">get started now</a>
						</li>

						<li class="gold wow flipInY animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
							<h3>Gold</h3>
							<ul class="benefits-list">
								<li>Responsive</li>
								<li>Documentation</li>
								<li>Multiplatform</li>
								<li>Video background</li>
								<li>Support</li>
								<li class="special-price">from <strong>$7.99</strong> /month</li>
							</ul>
							<a href="#" class="btn btn-default btn-download">get started now</a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
	</section>