<!-- ****************************** Specialty section ************************** -->
	<section id="features" class="block overflow-visible">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Наши преимущества</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			
			<div class="row special-card-wraper">
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-android-subway"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Официально работающее ТОО</h3>
							<p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-ios-monitor"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Готовое оборудование</h3>
							<p>Оборудование куплено и настроено, осталось лишь выбрать тарифный план</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-arrow-resize"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Контроль добычи 24/7</h3>
							<p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-ios-gear"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Удобство майнинга</h3>
							<p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>