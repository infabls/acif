<!-- ****************************** Contact section ************************** -->
	<section id="contact" class="block">
			<div class="container">
				<div class="title-area text-center">
					<h1 class="section-title">У вас остались вопросы?</h1>
					<hr class="title-under-line-top" />
					<hr class="title-under-line-bottom" />
				</div>
				<div class="contact-introductory-text text-center col-md-8 col-md-offset-2">
					ACIF это гибкая платформа ссотрудничества. Есть уникальные идеи, но нет средств? Тогда вам к нам! Есть средства, но не знаете как их преумножить? Напишите нам - и мы ответим на ваши вопросы!
				</div>
				<div class="row">
					<div id="response" class="col-md-11 col-md-offset-1"></div>
					<form id="contact_form" action="php/contact.php" method="post">
						<div class="col-md-5 col-md-offset-1">
							<div class="form-group">
							    <input type="name" class="form-control height-50" id="name" name="name" placeholder="Как к вам обращаться?" required>
							</div>
							<div class="form-group">
							    <input type="email" class="form-control height-50" id="email" name="email" placeholder="Ваш Email" required>
							</div>
						</div>
						<div class="col-md-6">
							<textarea class="form-control" placeholder="Ваше сообщение" name="message" id="message"></textarea>
						</div>
						<div class="col-md-12 text-center submit-btn-wraper">
							<button type="submit" name="submit" id="submit" class="btn btn-default btn-download btn-contact">Отправить<i class="ion-ios-arrow-thin-right icon-margin"></i></button>
						</div>
					</form>
				</div><!--/row-->
			</div>
			<div class="clearfix"></div>
	</section>