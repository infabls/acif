<section id="platform" class="text-center">
<div class="container">
	<h2>О платформе</h2>
		<p>Торгово – инвестиционная платформа
ACIF.KZ предоставляет удобные
возможности преумножить капитал, либо
помочь проекту произвести какой либо
продукт и получив от этого обоюдную
выгоду в виде уникального товара.</p>
	<div class="row">	
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="card-about">
				<img src="" alt="">
				<h3>Облачный майнинг</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro veritatis in nesciunt rerum eaque cum accusantium necessitatibus, voluptatum similique quisquam eius sit odio rem minus quo animi blanditiis natus nostrum.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="card-about">
				<img src="" alt="">
				<h3>Краудфандинг</h3>
				<p>Краудфандинг - это объединение средств людей для создания бизнеса, не подземное с помощью частного капитала, мы предлагаем гибкие условия как для инвесторов, так и для разработчиков проектов, присоединяйтесь сейчас и мы построим вместе лучшее будущее! </p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="card-about">
				<img src="" alt="">
				<h3>Обучение</h3>
				<p>Eveniet ullam tempore, culpa ad dolor laudantium, omnis maxime quam repellat ducimus doloremque! Illum obcaecati repellat, veniam esse sequi, praesentium sunt. Pariatur, facilis voluptas. Cum ad quod ab voluptas dolorem!</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2>
				О нашей команде
			</h2>
			<p>
				Наша команда с 2016 года занимается добычей и торговлей на рынке криптоактивов. На текущий момент мы решили поделиться нашими наработками и предлагаем Вам присоедениться к команде криптоэнтузиастов присоеденившись к пулу облачного майнинга. Так же наша платформа позволит Вам вложиться посредством ICO в новые перспективные стартапы и самим запускать их (условия оговариваются лично при встрече). Методы заработка и преумножения Вашего капитала постоянно будут пополняться. Мы работаем с инвесторами как посредством встреч, так и через интернет. Так же через нашу платформу вы можете пожертвовать средства на благотворительные цели, на данный момент это – Школа Добрых Дел. Благодарим за внимание, надеемся на дальнейшее сотрудничество.
			</p>
		</div>
	</div>
</div>
</section>