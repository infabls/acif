<?php 
require_once '../frame/modules/user/referal.php';
$img_url = 'https://acif.kz/public/img/bg_mining.jpg';
$url = htmlspecialchars($_SERVER['REQUEST_URI'])
?>

<!DOCTYPE html>
<html lang="ru">
<head>

	<!-- BASE -->
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<!-- PWA -->
	<link rel="manifest" href="manifest.json">
	<meta name="theme-color" content="#960605">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="msapplication-navbutton-color" content="#656565">
	<meta name="apple-mobile-web-app-status-bar-style" content="#656565">

	<!-- SMM -->
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:description" content="<?php echo $descr; ?>">
	<meta property="og:image" content="<?php echo $img_url; ?>">
	<meta property="og:type" content="article"> 
	<meta property="og:url" content="<?php echo $url; ?>">
	<meta itemprop="image" content="<?php echo $img_url; ?>">

	<!-- SEO -->
	<meta name="robots" content="index, follow">
	<title><?php echo $title; ?></title>
	<meta name="Description" content="<?php echo $descr; ?> ">
	<meta name="Keywords" content="<?php echo $Keywords ?>">





<link rel="stylesheet" href="css/libs/bootstrap.min.css">
<!-- LINK -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<!-- Stylesheets -->
	<link rel="stylesheet" href="libs/ionicons/css/ionicons.min.css">
	
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	
	<link rel="stylesheet" href="css/nivo-lightbox/nivo-lightbox.css">
	<link rel="stylesheet" href="css/nivo-lightbox/nivo-lightbox-theme.css">

	<link rel="stylesheet" href="css/magnific-popup.css">

	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/style.css">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

	<!-- Modernizr to provide HTML5 support for IE. -->
	<script src="js/modernizr.custom.js"></script>


</head>
<body id="home">

	<!-- ****************************** Preloader ************************** -->
	<div id="preloader"></div>


	<!-- ****************************** Sidebar ************************** -->
	<nav id="sidebar-wrapper">
		<a id="menu-close" href="#" class="close-btn toggle">ЗАКРЫТЬ <i class="ion-log-in"></i></a>
	    <ul class="sidebar-nav">
		    <li><a href="/index.php">Главная</a></li>
			<li><a href="#video">Video</a></li>
			<li><a href="#bigfeatures">Features</a></li>
			<li><a href="#features">Specialty</a></li>
			<li><a href="#features-left-image">With Image</a></li>
			<li><a href="#testimonial">Testimonial</a></li>
			<li><a href="#team">Team</a></li>
			<li><a href="#pricing">Pricing</a></li>
			<li><a href="#subscribe">Subscribe</a></li>
			<li><a href="#contact">Contact us</a></li>
	    </ul>
	</nav>

	
	<!-- ****************************** Header ************************** -->
	<header class="sticky" id="header">
		<div class="container">
			<div class="row" id="logo_menu">
				<div class="col-xs-6"><a class="logo" href="/"><img src="img/Logo.png" alt=""></a></div>
				<div class="col-xs-6"><a id="menu-toggle" href="#" class="toggle" role="button" title="Меню" data-toggle="tooltip" data-placement="left"><i class="ion-navicon"></i></a></div>
			</div>
		</div>
	</header>