	<!-- ****************************** Footer ************************** -->
	<section id="footer" class="block">
		<div class="container text-center">
			<div class="footer-logo">
				<a class="logo" href=""><img src="img/Logo.png" alt=""></a>
			</div>
			<div class="copyright">
				&copy;2017 Acif.kz Designed by <span class="text-green"><a href="https://infabls.com" target="_blank" rel="follow">Infabls.com</a></span>
			</div>
		</div><!-- container -->
	</section>



	<!-- ****************************** Back to top ************************** -->
	<a id="back-to-top" href="#top" class="btn btn-primary btn-lg back-to-top" role="button" title="Нажмите, чтобы вернуться наверх страницы" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>



	<!-- All the scripts -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="libs/bootstrap/js/bootstrap.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/contact.js"></script>
	<script src="js/jquery.easy-ticker.js"></script>
	<script src="js/jquery.magnific-popup.js"></script>
</body>
</html>