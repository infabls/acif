<!-- ****************************** Testimonial ************************** -->
	<section id="testimonial" class="block overflow-visible">
		<div class="container">		
			<div id="review" class="owl-carousel owl-theme wow animated bounceInUp">
				<div class="item">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
							<div class="client-pic"><img class="img_res" src="img/client-two.png" alt=""></div>
							<p class="client-name">
								Shahjahan Jewel
							</p>
							<p class="review-star">
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star-outline"></i>
							</p>
							<p class="review-desc">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip.
							</p>
							<p class="testimonial-date">January 28, 2016</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-sm-8 col-md-offset-2">
							<div class="client-pic"><img class="img_res" src="img/client-three.png" alt=""></div>
							<p class="client-name">
								Jane Doe
							</p>
							<p class="review-star">
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star"></i>
								<i class="ion-star-outline"></i>
							</p>
							<p class="review-desc">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip.
							</p>
							<p class="testimonial-date">January 28, 2016</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>