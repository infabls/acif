<!-- ****************************** Banner ************************** -->
	<section id="banner" >
		<div class="banner-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-6 hidden-xs hidden-sm">
					<div class="hand-container">
					<img class="iphone-hand img_res wow animated bounceInUp" data-wow-duration="1.2s" src="img/Assets.png" alt="">
					<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="headings">
						<h1 class="wow animated fadeInDown hidden-xs">
							<strong>Платформа</strong>
							Криптотехнологий и 
							Блокчейна
						</h1>
						<p class="wow animated fadeInLeft">Начните <strong>зарабатывать на криптовалюте</strong> вместе с нами уже сегодня! Регистрируйтесь и развивайте свои навыки криптотрейдинга.</p>
				
						<a href="user/login" class="btn btn-store wow animated bounceInUp">
							<button class="btn btn-default btn-download">Вход</button
						</a>
						<a href="user/register" class="btn btn-store wow animated bounceInUp">
							<button class="btn btn-default btn-download">Регистрация</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>