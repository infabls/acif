<!-- ****************************** Big Features section ************************** -->
	<section id="bigfeatures" class="img-block-3col block">

		<div class="container">

			<div class="title-area text-center">
				<h1 class="section-title">Что мы предлагаем?</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>


			<div class="row margin-top-50">
				<div class="col-sm-4 pad-right-85">
					<ul class="item-list-right item-list-big">
						<li class="wow fadeInLeft animated"> 
						    <div style="display: inline-block;">
	                            <div style="float: left;">
								  	<h3 class="margin-top-right">Облачный майнинг</h3>
								</div>
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-cloud"></i></span>
							        </div>
							    </div>
                            </div>
							<p>Начните зарабатывать на технологии облачного майнинга уже сегодня</p>
						</li>
						<li class="wow fadeInLeft animated"> 
							<div style="display: inline-block;">
	                            <div style="float: left;">
								  	<h3 class="margin-top-right">Академия блокчейна</h3>
								</div>
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-android-color-palette"></i></span>
							        </div>
							    </div>
                            </div>
							<p>Желаете разобраться в рынке криптовалют? Приходите учиться в нашу академию!</p>
						</li>
						<li class="wow fadeInLeft animated"> 
							<div style="display: inline-block;">
	                            <div style="float: left;">
								  	<h3 class="margin-top-right">Блог и статьи</h3>
								</div>
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-android-hand"></i></span>
							        </div>
							    </div>
                            </div>
							<p>Получайте первыми новости, мнения экспертов и инсайды о мире криптовалюты!</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-push-4 pad-left-85">
					<ul class="item-list-left item-list-big">
						<li class="wow fadeInRight animated condensed-icon"> 
							<div style="display: inline-block;">
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-android-unlock"></i></span>
							        </div>
							    </div>
							    <div style="float: left;">
								  	<h3 class="margin-top-left">Личный кабинет</h3>
								</div>
                            </div>
							<p>Подробная аналитика вложений. Удобный ввод и вывод средств. Максимальная безопасность ваших данных.</p>
						</li>
						<li class="wow fadeInRight animated condensed-icon">
							<div style="display: inline-block;">
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-arrow-resize"></i></span>
							        </div>
							    </div>
							    <div style="float: left;">
								  	<h3 class="margin-top-left">Инвестиции в стартапы</h3>
								</div>
                            </div>
							<p>Преумножайте свои деньги инвестируя в перспективные проекты.</p>
						</li>
						<li class="wow fadeInRight animated">
							<div style="display: inline-block;">
								<div class="test-2">
							    	<div class="icon-test">
							        	<span class="x"><i class="ion-android-options"></i></span>
							        </div>
							    </div>
							    <div style="float: left;">
								  	<h3 class="margin-top-left">Биржи и монеты</h3>
								</div>
                            </div>
							<p>Будьте в курсе изменений курсов валют. Покупайте монеты по лучшей цене.</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-pull-4 text-center">
					<div class="animation-box wow bounceIn animated">
						<img class="highlight-left wow animated" src="img/spark.png" height="192" width="48" alt=""> 
						<img class="highlight-right wow animated" src="img/spark.png" height="192" width="48" alt="">
						<img class="screen" src="img/03.png" alt="" height="581" width="300">
					</div>
				</div>
			</div>
		</div>
	</section>