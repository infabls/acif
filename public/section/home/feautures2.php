<!-- ****************************** Specialty section ************************** -->
	<section id="features" class="block overflow-visible">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Почему именно мы?</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			
			<div class="row special-card-wraper">
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-android-subway"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Низкий порог для инвестиций</h3>
							<p>Высококонкурентные цены на облачные разработки и минимальная сумма депозита 200$.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-ios-monitor"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Реферальная программа</h3>
							<p>Зарабатывайте деньги за регистрацию новых пользователей, пришедших по вашей ссылке.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-arrow-resize"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Моментальный вывод</h3>
							<p>Вывод средств доступен сразу после начисления. Вывод на карту, эл. кошельки, криптовалюту.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3 special-card-outer">
					<div class="feature-box wow animated flipInX animated special-card text-center">
						<div class="card-icon-box">
							<div class="card-icon-box-inner">
								<i class="ion-ios-gear"></i>
							</div>
						</div>
						<div class="card-content">
							<h3 class="card-title">Прозрачность комиссий</h3>
							<p>Наш сервис не содержит скрытых платежей и комиссий. Все наши комиссии можно посмотреть на странице <a href="price">Цены на услуги</a></p>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>