<!-- ****************************** Team section ************************** -->
	<section id="team" class="block">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Наша команда</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			<div class="row developer-area-wraper">
				<div class="col-md-6 col-sm-12">
					<div class="row developer-card">
						<div class="col-md-6 col-sm-6 no-pad-left-right">
							<img class="img-res" src="img/Chairpersons_2.png" alt="">
						</div>
						<div class="col-md-6 col-sm-6 text-center developer-desc">
							<p class="developer-name">Smith Mehra</p>
							<p class="developer-designation">Graphics Designer</p>
							<ul class="team-social">
								<li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
								<li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
								<li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row developer-card">
						<div class="col-md-6 col-sm-6 no-pad-left-right">
							<img class="img-res" src="img/Chairpersons_3.png" alt="">
						</div>
						<div class="col-md-6 col-sm-6 text-center developer-desc">
							<p class="developer-name">Wiliams White</p>
							<p class="developer-designation">Apps Developer</p>
							<ul class="team-social">
								<li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
								<li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
								<li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row developer-card">
						<div class="col-md-6 col-sm-6 no-pad-left-right">
							<img class="img-res" src="img/Chairpersons_1.png" alt="">
						</div>
						<div class="col-md-6 col-sm-6 text-center developer-desc">
							<p class="developer-name">Snikda Chacra</p>
							<p class="developer-designation">Apps Developer</p>
							<ul class="team-social">
								<li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
								<li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
								<li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row developer-card">
						<div class="col-md-6 col-sm-6 no-pad-left-right">
							<img class="img-res" src="img/tm2.png" alt="">
						</div>
						<div class="col-md-6 col-sm-6 text-center developer-desc">
							<p class="developer-name">Alia Nehra</p>
							<p class="developer-designation">Apps Developer</p>
							<ul class="team-social">
								<li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
								<li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
								<li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
								<li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>