<!-- ****************************** Left Image section ************************** -->
	<section id="features-left-image" class="block">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-6 wow fadeInLeft animated">
					<div class="phone-image">
						<img class="img-responsive" src="img/Assets.png" alt="">
					</div>
				</div><!--/col-sm-6-->

				<div class="col-sm-6 wow fadeInRight animated">
					
					<div class="title-area text-center">
						<h1 class="section-title">Возможности личного кабинета</h1>
					</div>
					
					<div class="row">
						<div class="col-md-12 causes-content">
							<p>Мы разработали личный кабинет для наших пользователей. Мы постарались учесть и реализовать необходимые функции, в которых нуждаются люди, увлеченные миром криптовалюты. Используя наши инструменты, вы минимизируете риски</p>
							<ul class="feature-list">
								<li><i class="ion-locked"></i> Надежная и безопасная платформа</li>
								<li><i class="ion-person-stalker"></i> Прибыльная реферальная программа</li>
								<li><i class="ion-arrow-swap"></i> Удобный ввод и вывод средств</li>
								<li><i class="ion-cloud"></i> Работает на основе облачных технологий </li>
							</ul>
							<a href="user/login" class="btn btn-default btn-download">Подробнее<i class="ion-ios-arrow-thin-right icon-margin"></i></a>
						</div>
					</div>
				</div><!--/col-sm-6-->
				<div class="col-md-12 text-center bottom-img-wraper">
					<img src="img/iPhone 6 - Gold copy.png" alt="">
				</div>
			</div><!--/row-->
			
		</div><!--/container-->
	</section>