<!-- ****************************** Video section ************************** -->
	<section id="video" class="block overflow-visible">
		<div class="container">
			<div class="title-area text-center">
				<h1 class="section-title">Познакомьтесь ближе с нашей платформой</h1>
				<hr class="title-under-line-top" />
				<hr class="title-under-line-bottom" />
			</div>
			<div class="row clearfix video-area-wraper">
				<div class="col-md-8 forward-div">
					<div class="video-box">
						<div class="video-play-btn">
							<a class="ply-btn video" title="Lucy App"  href="https://vimeo.com/123123">
								<i class="ion-ios-play"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-md-push-4 video-caption">
					<div class="row caption-row">
						<div class="col-md-6 col-md-push-6 caption-inner clearfix">
							<div class="caption-content">
								<p>
									Acif.kz – облачный сервис, который объединяет людей, увлеченных миром криптовалюты. Станьте и вы нашим партнером.
								</p>
								<a href="user/login" class="btn btn-default btn-download">Присоединиться <!-- <i class="ion-ios-arrow-thin-right icon-margin"></i> --></a>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>