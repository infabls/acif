<?php  
// реферальный код содержится в get-параметре, нам надо сохранить его
// до того времени, как пользователь произведет нужное действие, тогда можно будет сказать, что
// он пришел от такого-то реферала. используем сессию:
session_start();
if (!isset($_SESSION['ref']) && isset($_GET['ref'])) {
  $_SESSION['ref'] = $_GET['ref'];
}
 // мавр сделал дело, сохраняем результат
//$ref = isset($_SESSION['ref']) ? mysqli_real_escape_string($link, $_SESSION['ref']) : '';
//mysqli_query($link, "INSERT INTO `foo`(..., `ref`) VALUES (..., '{$ref}')");
?>